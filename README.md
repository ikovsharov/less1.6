# less1.6

# Описание задания
Написать docker-compose.yml со сл условиями:
1. 3 контейнера nginx1 nginx2 grafana (https://hub.docker.com/r/grafana/grafana)
2. Nginx1 port 8001 на Grafana
3. Nginx2 port 8002 на Grafana
4. Grafana не пробрасывать ничего наружу, не должна подниматься при остановке руками.
5. Nginx1 и Nginx2 в разных сетях.
6. И конечно 2 конфига нжинкса которые будут пробрасывать на Grafana (пример конфиг с лес1.1 джанго конф)

Доп.задача:
Сделать хелсчек Nginx1 в контейнере с Grafana
